//Cathy Tham 1944919
package lab02;

public class BikeStore {

	public static void main(String[] args) {
		Bicycle[] bikes=new Bicycle[4];
		bikes[0]=new Bicycle("Specialized",21,40);
		bikes[1]=new Bicycle("Focus",25,60);
		bikes[2]=new Bicycle("Raleigh",23,55);
		bikes[3]=new Bicycle("Trek",19,45);
		
		printArr(bikes);

	}
	
	 private static void printArr(Bicycle[] bikes) {
		 for (int i=0; i<bikes.length; i++) {
			 System.out.println(bikes[i].toString());
		 }
	 }
}
