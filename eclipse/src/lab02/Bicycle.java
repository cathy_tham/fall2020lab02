//Cathy Tham 1944919

package lab02;

public class Bicycle {
	private String manufacturer;
	private int numberGears;
	private double maxSpeed;
	
	//constructor
	public Bicycle(String manufacturer, int numberGears, double maxSpeed){
		this.manufacturer= manufacturer;
		this.numberGears=numberGears;
		this.maxSpeed=maxSpeed;	
	}
	
	//get methods
	public String getManufacturer(){
		return manufacturer;
	}
	
	public int getNumberGears(){
		return numberGears;
	}
	
	public double getMaxSpeed(){
		return maxSpeed;
	}
	
	//overwrite the toString() 
	public String toString(){
		return("Manufacturer: "+ manufacturer+", Number of Gears: "+numberGears+ ", MaxSpeed: "+ maxSpeed);	
	}
	
}
  
